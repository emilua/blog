= Emilua 0.8 released
:revdate: 2024-05-20

The main change was re-licensing the project to be dual-licensed. Now you choose
between BSL-1.0 or MIT. Previously it was licensed under BSL-1.0 (not a popular
choice within the Lua community).

Emilua core is already heavily feature packed. For many areas (threading, async
IO, concurrent IO, sandboxing, ...) it's years ahead of any other Lua
solution. Still... it has been failing spectacularly to attract users. This can
only mean Emilua doesn't need new features. Features is not what will attract
users. So I made a decision: as of now, Emilua is in maintenance mode. The
project will receive bugfixes and changes to support newer releases of the
dependencies it uses (e.g. breaking changes in libboost), but development of the
core package will pretty much crawl to a slow sluggish pace.

You can find the full changelog at
<https://docs.emilua.org/api/0.8/changelog.html>.

As usual, it'll take a few days to release the pre-built Windows binaries for
those not willing to compile the project from scratch. Packages for Linux
distros and FreeBSD take longer as the changes need to be reviewed and accepted.
